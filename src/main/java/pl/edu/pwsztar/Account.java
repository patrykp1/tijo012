package pl.edu.pwsztar;

public class Account {
    private Integer accountNumber;
    private Integer deposit =0;

    public Account(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", deposit=" + deposit +
                '}';
    }
}
