package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private int accountNumber = 0;

    List<Account> accounts = new ArrayList<Account>();

    public int createAccount() {
        accountNumber++;
        System.out.println("TWORZE KONTO: " + accountNumber);
        accounts.add(new Account(accountNumber));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {

        System.out.println("USUWAM KONTO " + accountNumber);
        if(accountNumber<0){
            return ACCOUNT_NOT_EXISTS;
        }
        for(Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                accounts.remove(account);
                return account.getDeposit();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        //System.out.println("KONTA: " + accounts);
        for (Account account : accounts) {
           // System.out.println("Konto: "  +  account.toString());
            if (account.getAccountNumber() == accountNumber) {
                if(amount<0){
                    return false;
                }
                account.setDeposit(amount);
                return true;
            }
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        //System.out.println("Dostepne konta :"  + accounts);
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber && amount>0) {
                //if (account.getAccountNumber() == 1) {System.out.println("mam:" + account.getDeposit() +" odejmuje " + amount);}
                if(account.getDeposit()>=amount){
                    account.setDeposit(account.getDeposit()-amount);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        boolean source = false;
        boolean destination = false;

        if(amount<0){
            return false;
        }

        for (Account account : accounts) {
            if (account.getAccountNumber() == fromAccount) {
                if(account.getDeposit()-amount>=0){
                    source=true;
                }
            }
            if (account.getAccountNumber() == toAccount) {
                destination=true;
            }
        }
        if(source&&destination) {
            for (Account account : accounts) {
                if (account.getAccountNumber() == fromAccount) {
                    System.out.println("Odejmuje z konta numer " + account.getAccountNumber() +  " kwote " + amount );
                    account.setDeposit(account.getDeposit()-amount);
                }
                if (account.getAccountNumber() == toAccount) {
                    System.out.println("Dodaje do konta " + account.getAccountNumber() +  " kwote " + amount );
                    account.setDeposit(account.getDeposit()+amount);
                }
            }
            return true;
        }

        return false;
    }

    public int accountBalance(int accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber() == accountNumber) {
                return account.getDeposit();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        int sum=0;
        for (Account account : accounts) {
            sum+=account.getDeposit();
        }
        return sum;
    }
}
